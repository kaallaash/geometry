Короткая инструкция:
1. Кидаешь кубики, вводишь координаты X и Y ==>
Левый нижний край фигуры становится на указанные координаты
2. Игра заканчивается если у одного из игроков заполнена большая половина поля
или если заканчиваются шансы перекинуть кубик
3. Выигрывает тот игрок у которого по итогу большее кол-во очков

Что бы проще протестить функционал конца игры - можно изменить в коде минимальные габариты поля
Settings.cs ==> line 58  - значение 30 изменить на 12(меньше не нужно) 
line 73  - значение 20 изменить на 12 (меньше не нужно) 
почему не нужно меньше , игра создавалась для больших габаритов
и может не дать перекинуть кубики, т.к. для перекидки кубиков - нужно минимум треть заполненного поля