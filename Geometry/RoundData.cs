﻿namespace Geometry
{
    public class RoundData
    {
        public int Player { get; set; }
        public int XTip { get; set; }
        public int YTip { get; set; }
        public int RoundGame { get; set; }
        public int[] CountDice { get; set; }
    }
}
