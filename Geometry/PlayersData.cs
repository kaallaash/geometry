﻿namespace Geometry
{
    public class PlayersData
    {
        public string[,] PlayersField { get; set; }
        public int[] PlayerAreaPoints { get; set; }
        public int[] Chance { get; set; }
        public int[] Tip { get; set; }
    }
}
