﻿namespace Geometry
{
    public interface IMessage
    {
        void ShowLanguage();

        void RepeatChooseLanguage();

        void SetGabarits();

        void SetWidth();

        void RepeatSetWidth();

        void SetHeight();

        void RepeatSetHeight();

        void LineStats(int playerAreaPoints, int tip, int chance);

        void PressAnyKey(int player);

        void NoMovesRollDice();

        void NoMoves();

        void SetСoordinates();

        void SetСoordinateX();

        void SetСoordinateY();

        void RepeatSetСoordinates(int x, int y);

        void RepeatSetСoordinates(int x, int y, ref int attemp);

        void CheerUp();

        void UseTip();

        void GoodChoice();

        void Win(int player, int[] square);
    }
}
