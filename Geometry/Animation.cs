﻿using System;
using System.Threading;

namespace Geometry
{
    class Animation
    {
        public static void ShowAK()
        {
            InTheEndA();
            InTheEndK();
        }

        private static void InTheEndA()
        {
            int height = 29;

            for (int i = 0; i < height; i++)
            {
                if (15 - i > 2)
                {
                    Console.SetCursorPosition(15 - i, i);
                    Console.WriteLine("By Andrey K");
                    Thread.Sleep(40);
                }
                else
                {
                    Console.SetCursorPosition(2, i);
                    Console.WriteLine("By Andrey K");
                    Thread.Sleep(40);
                }

            }

            for (int i = 0; i < height; i++)
            {
                if (15 + i < 27)
                {
                    Console.SetCursorPosition(15 + i, i);
                    Console.WriteLine("By Andrey K");
                    Thread.Sleep(40);
                }
                else
                {
                    Console.SetCursorPosition(27, i);
                    Console.WriteLine("By Andrey K");
                    Thread.Sleep(40);
                }
            }

            for (int i = 0; i < 3; i++)
            {
                Console.SetCursorPosition(14, 19 + i);
                Console.WriteLine("By Andrey K");
                Thread.Sleep(40);
            }
        }

        private static void InTheEndK()
        {
            int height = 28;
            for (int i = 0; i < height; i++)
            {
                if (20 - i == 2)
                {
                    break;
                }
                Console.SetCursorPosition(55 - i, height - i);
                Console.WriteLine("By Andrey K");
                Thread.Sleep(40);
            }

            for (int i = 0; i < height; i++)
            {
                if (20 - i == 2)
                {
                    break;
                }
                Console.SetCursorPosition(55 - i, i);
                Console.WriteLine("By Andrey K");
                Thread.Sleep(40);
            }

            for (int i = 0; i <= height; i++)
            {
                Console.SetCursorPosition(37, height - i);
                Console.WriteLine("By Andrey K");
                Thread.Sleep(40);
            }
        }

        public static void ShowWinAnimation(IMessage message, RoundData roundDataPlayer, PlayersData playersData)
        {
            for (int i = 0; i < 8; i++)
            {
                message.Win(roundDataPlayer.Player, playersData.PlayerAreaPoints);
                Animation.ShowWin(roundDataPlayer.Player);
            }
        }

        public static void ShowWin(int player)
        {
            Thread.Sleep(250);

            if (player == 0)
            {
                Settings.ChangeColor(ConsoleColor.Green);
            }
            else
            {
                Settings.ChangeColor(ConsoleColor.Red);
            }

            ShowWinLatterW();
            ShowWinLatterI();
            ShowWinLatterN();
            ShowWinExclamation();
            Thread.Sleep(250);
            Settings.ChangeColor(ConsoleColor.Black);
        }

        private static void WriteWin(int pos, int line)
        {
            Console.SetCursorPosition(line, pos);
            Console.WriteLine("Win");
        }

        private static void ShowWinLatterW()
        {
            for (int i = 6; i < 10; i++)
            {
                WriteWin(i, 14);
            }

            for (int i = 9; i < 13; i++)
            {
                WriteWin(i, 15);
            }

            for (int i = 11; i < 15; i++)
            {
                WriteWin(i, 16);
            }

            for (int i = 8; i < 12; i++)
            {
                WriteWin(i, 19);
            }

            for (int i = 11; i < 15; i++)
            {
                WriteWin(i, 22);
            }

            for (int i = 9; i < 13; i++)
            {
                WriteWin(i, 23);
            }

            for (int i = 6; i < 10; i++)
            {
                WriteWin(i, 24);
            }
        }

        private static void ShowWinLatterI()
        {
            for (int i = 6; i < 15; i++)
            {
                if (i == 6 || i == 7 || i == 13 || i == 14)
                {
                    WriteWin(i, 29);
                }
            }

            for (int i = 8; i < 13; i++)
            {
                WriteWin(i, 30);
            }

            for (int i = 6; i < 15; i++)
            {
                if (i == 6 || i == 7 || i == 13 || i == 14)
                {
                    WriteWin(i, 31);
                }
            }
        }

        private static void ShowWinLatterN()
        {
            for (int i = 6; i < 15; i++)
            {
                WriteWin(i, 36);
            }

            WriteWin(7, 37);
            WriteWin(8, 38);
            WriteWin(9, 38);
            WriteWin(10, 39);
            WriteWin(11, 40);
            WriteWin(12, 41);
            WriteWin(13, 41);

            for (int i = 6; i < 15; i++)
            {
                WriteWin(i, 42);
            }
        }

        private static void ShowWinExclamation()
        {
            for (int i = 6; i < 15; i++)
            {
                if (i != 12)
                {
                    WriteWin(i, 47);
                }

            }
        }
    }
}
