﻿using System;
using System.Threading;

namespace Geometry
{
    public class Settings
    {
        const int countWorkLinesWithoutField = 22;
        const string fillingWorkField = "0";
        const string fillingShowField = "·";

        public static IMessage GetMessagesLanguage()
        {
            Console.WriteLine("Приветствуем в игре!!!");
            Thread.Sleep(1000);
            Console.WriteLine("Выберите Язык: \n1 - Русский \n2 - English");
            IMessage message;
            for (; ; )
            {
                string language = Console.ReadLine();

                if (language == "1")
                {
                    message = new RuMessage();
                    break;
                }
                else if (language == "2")
                {
                    message = new EngMessage();
                    break;
                }
                else
                {
                    Console.WriteLine("Выберите правильно язык, клавиша \"1\" или клавишу \"2\"");
                }                
            }

            message.ShowLanguage();
            Thread.Sleep(800);
            return message;
        }

        public static RoundData CreateRoundData()
        {
            return new RoundData { Player = 0, XTip = 0, YTip = 0, RoundGame = 1, CountDice = new int[2] };
        }

        public static FieldSize SetGabarits(IMessage message)
        {
            int height;
            int width;

            message.SetGabarits();
            message.SetWidth();
            for (; ; )
            {
                string strWidth = Console.ReadLine();
                if (CheckLength(strWidth, 30 , 50))
                {
                    width = Convert.ToInt32(strWidth);
                    break;
                }
                else
                {
                    message.RepeatSetWidth();
                }
            }

            message.SetHeight();
            for (; ; )
            {
                string strHeight = Console.ReadLine();
                if (CheckLength(strHeight, 20, 40))
                {
                    height = Convert.ToInt32(strHeight);
                    break;
                }
                else
                {
                    message.RepeatSetHeight();
                }
            }
            return new FieldSize { Height = height, Width = width };
        }

        public static PlayersData CreatePlayers(FieldSize field)
        {

            return new PlayersData { PlayerAreaPoints = new int[] { 0, 0 }, Chance = new int[] { 1, 1 }, Tip = new int[] { 1, 1 }, PlayersField = CreateFild(field, fillingWorkField) };
        }

        public static void StartPlay(IMessage message, FieldSize field, RoundData roundDataPlayer, PlayersData playersData)
        {
            Console.Clear();
            Console.WriteLine("\n");

            string[,] showField = Settings.CreateFild(field, fillingShowField);

            for (; ; )
            {
                Settings.ShowField(showField, playersData, message);
                RollDice(out roundDataPlayer.CountDice[0]);
                message.PressAnyKey(roundDataPlayer.Player);
                Console.ReadKey();
                Console.Write("\b ");
                RollDice(out roundDataPlayer.CountDice[1]);

                ShowDice(roundDataPlayer.CountDice[0], roundDataPlayer.CountDice[1], field.Height);

                if (roundDataPlayer.RoundGame == 2 && CheckField(playersData, ref roundDataPlayer) == false && playersData.Chance[roundDataPlayer.Player] == 1)
                {
                    playersData.Chance[roundDataPlayer.Player]--;

                    Console.Clear();
                    message.NoMovesRollDice();
                    Console.WriteLine();
                    continue;
                }
                else if (roundDataPlayer.RoundGame == 2 && CheckField(playersData, ref roundDataPlayer) == false && playersData.Chance[roundDataPlayer.Player] == 0)
                {
                    message.NoMoves();
                    Thread.Sleep(400);
                    break;
                }

                Settings.SetCoordinates(message, field, ref playersData, ref roundDataPlayer, playersData.PlayersField, showField, out int x, out int y);

                playersData.PlayerAreaPoints[roundDataPlayer.Player] += roundDataPlayer.CountDice[0] * roundDataPlayer.CountDice[1];

                ReplaceRoundIfTrue(field, playersData, ref roundDataPlayer);
                ReplaceFild(ref playersData, roundDataPlayer, x, y);

                if (CheckPlayerAreaPointsMoreThanSquareFieldDividedTwo(field, playersData))
                {
                    break;
                }

                ChangePlayer(ref roundDataPlayer);
            }
        }

        public static void SetConsoleHeight(int height)
        {
            int ConsoleHeight = Console.WindowHeight;

            for (int i = height + countWorkLinesWithoutField; i >= ConsoleHeight; i--)
            {
                try
                {
                    Console.WindowHeight = i;
                    break;
                }
                catch { }
            }
        }

        private static bool CheckLength(string strLenght, int min, int max)
        {
            try
            {
                var width = Convert.ToInt32(strLenght);
                if (width < min || width > max)
                {
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static void SetCoordinates(IMessage message, FieldSize field, ref PlayersData playersData, ref RoundData roundDataPlayer, string[,] workField, string[,] showField, out int x, out int y)
        {
            int inputAttemptCoordinats = 0;
            message.SetСoordinates();

            for (; ; )
            {
                for (; ; )
                {
                    message.SetСoordinateX();
                    string xString = Console.ReadLine();
                    message.SetСoordinateY();
                    string yString = Console.ReadLine();
                    if (CheckСoordinates(xString, out x, yString, out y, field.Width, field.Height))
                    {
                        break;
                    }
                    else if (inputAttemptCoordinats == 0)
                    {
                        message.RepeatSetСoordinates(field.Width, field.Height);
                        inputAttemptCoordinats++;
                    }
                    else
                    {
                        ClearThreeLastString(Console.WindowHeight - 1);
                        message.RepeatSetСoordinates(field.Width, field.Height, ref inputAttemptCoordinats);
                    }
                }

                if (CheckArrayBorder(workField, roundDataPlayer, x, y) && CheckArrayForEmptyCell(workField, roundDataPlayer, x, y))
                {
                    Console.Clear();
                    message.CheerUp();
                    Console.WriteLine();
                    break;
                }
                else if (inputAttemptCoordinats == 0)
                {
                    message.RepeatSetСoordinates(field.Width, field.Height, ref inputAttemptCoordinats);
                }
                else if (roundDataPlayer.RoundGame == 2 && playersData.Tip[roundDataPlayer.Player] == 1 && inputAttemptCoordinats == 1)
                {
                    ClearThreeLastString(Console.WindowHeight - 1);

                    message.UseTip();
                    var chooseTip = Console.ReadLine();
                    if (chooseTip == "1")
                    {
                        playersData.Tip[roundDataPlayer.Player]--;
                        Console.Clear();
                        ShowTip(roundDataPlayer.XTip, roundDataPlayer.YTip);
                        Console.SetCursorPosition(0, 0);
                        message.GoodChoice();
                        Console.WriteLine();
                        Settings.ShowField(showField, playersData, message);
                        ShowDiceWithoutRoll(roundDataPlayer, field);
                        message.SetСoordinates();
                    }
                }
                else
                {
                    ClearThreeLastString(Console.WindowHeight - 1);
                    message.RepeatSetСoordinates(field.Width, field.Height, ref inputAttemptCoordinats);
                }
            }
        }

        public static void ShowField(string[,] showField, PlayersData playersData, IMessage message)
        {
            GetHeightWidtdArray(playersData.PlayersField, out int height, out int width);

            Console.Write("Y ^          ");
            ChangeColor(ConsoleColor.Green);
            Console.Write("···");
            ChangeColor(ConsoleColor.Black);
            message.LineStats(playersData.PlayerAreaPoints[0], playersData.Tip[0], playersData.Chance[0]);

            Console.Write("  |          ");
            ChangeColor(ConsoleColor.Red);
            Console.Write("···");
            ChangeColor(ConsoleColor.Black);
            message.LineStats(playersData.PlayerAreaPoints[1], playersData.Tip[1], playersData.Chance[1]);

            Console.WriteLine("  |");

            for (int i = 0; i < height; i++)
            {
                if (height - i < 10)
                {
                    Console.Write($"  {height - i} ");
                }
                else
                {
                    Console.Write($" {height - i} ");
                }

                for (int j = 0; j < width; j++)
                {
                    if (playersData.PlayersField[i, j] == "1")
                    {
                        ChangeColor(ConsoleColor.Green);
                        Console.Write($"{showField[i, j]} ");
                        ChangeColor(ConsoleColor.Black);

                    }
                    else if (playersData.PlayersField[i, j] == "2")
                    {
                        ChangeColor(ConsoleColor.Red);
                        Console.Write($"{showField[i, j]} ");
                        ChangeColor(ConsoleColor.Black);
                    }
                    else
                    {
                        Console.Write($"{showField[i, j]} ");
                    }
                }
                Console.WriteLine();
            }

            Console.Write(" ");
            for (int i = 0; i <= width; i++)
            {
                Console.Write($" {i / 10}");

            }
            Console.Write(" --> X");
            Console.WriteLine();

            Console.Write(" ");
            for (int i = 0; i <= width; i++)
            {
                Console.Write($" {i % 10}");

            }
            Console.WriteLine("\n");
        }

        private static bool CheckСoordinates(string xString, out int x, string yString, out int y, int width, int height)
        {
            try
            {
                x = Convert.ToInt32(xString);
                y = Convert.ToInt32(yString);

                if (x < 1 || x > width || y < 1 || y > height)
                {
                    return false;
                }
                return true;
            }
            catch
            {
                x = 0;
                y = 0;
                return false;
            }
        }

        private static bool CheckArrayBorder(string[,] workField, RoundData roundDataPlayer, int x, int y)
        {
            int height = workField.GetUpperBound(0) + 1;
            int width = workField.Length / height;
            if (x + roundDataPlayer.CountDice[0] - 1 <= width && y + roundDataPlayer.CountDice[1] - 1 <= height)
            {
                return true;
            }
            return false;
        }

        public static string[,] CreateFild(FieldSize field, string fillSymbol)
        {
            string[,] newArray = new string[field.Height, field.Width];

            for (int i = 0; i < field.Height; i++)
            {
                for (int j = 0; j < field.Width; j++)
                {
                    newArray[i, j] = fillSymbol;
                }
            }

            return newArray;
        }

        private static void ClearThreeLastString(int lastString)
        {
            Console.SetCursorPosition(0, lastString - 1);
            Console.Write("                                                            ");
            Console.SetCursorPosition(0, lastString - 2);
            Console.Write("                                                            ");
            Console.SetCursorPosition(0, lastString - 3);
            Console.Write("                                                            " +
                "                    ");
            Console.SetCursorPosition(0, lastString - 3);
        }

        private static void ShowTip(int xTip, int yTip)
        {
            Console.SetCursorPosition(35, 1);
            Console.Write($"Подсказка x:{xTip} y: {yTip}");
        }

        private static void ShowDiceWithoutRoll(RoundData roundDataPlayer, FieldSize field)
        {
            Dice(roundDataPlayer.CountDice[0], 0, field.Height + 10);
            Dice(roundDataPlayer.CountDice[1], 8, field.Height + 10);
        }

        static void Dice(int i, int pos, int line)
        {
            ChangeColor(ConsoleColor.Red);
            Console.SetCursorPosition(pos, line);

            if (i == 1)
            {
                Console.WriteLine("     ");
                Console.SetCursorPosition(pos, line + 1);
                Console.WriteLine("  o  ");
                Console.SetCursorPosition(pos, line + 2);
                Console.WriteLine("     ");
            }
            else if (i == 2)
            {
                Console.WriteLine("    o");
                Console.SetCursorPosition(pos, line + 1);
                Console.WriteLine("     ");
                Console.SetCursorPosition(pos, line + 2);
                Console.WriteLine("o    ");
            }
            else if (i == 3)
            {
                Console.WriteLine("    o");
                Console.SetCursorPosition(pos, line + 1);
                Console.WriteLine("  o  ");
                Console.SetCursorPosition(pos, line + 2);
                Console.WriteLine("o    ");
            }
            else if (i == 4)
            {
                Console.WriteLine("o   o");
                Console.SetCursorPosition(pos, line + 1);
                Console.WriteLine("     ");
                Console.SetCursorPosition(pos, line + 2);
                Console.WriteLine("o   o");
            }
            else if (i == 5)
            {
                Console.WriteLine("o   o");
                Console.SetCursorPosition(pos, line + 1);
                Console.WriteLine("  o  ");
                Console.SetCursorPosition(pos, line + 2);
                Console.WriteLine("o   o");
            }
            else
            {
                Console.WriteLine("o   o");
                Console.SetCursorPosition(pos, line + 1);
                Console.WriteLine("o   o");
                Console.SetCursorPosition(pos, line + 2);
                Console.WriteLine("o   o");
            }

            ChangeColor(ConsoleColor.Black);
        }

        public static bool CheckArrayForEmptyCell(string[,] workField, RoundData roundDataPlayer, int x, int y)
        {
            int rowUp = 0;
            int height = workField.GetUpperBound(0) + 1;
            for (int i = x; i < x + roundDataPlayer.CountDice[0] && rowUp < roundDataPlayer.CountDice[1]; i++)
            {
                if (workField[height - y - rowUp, i - 1] != "0")
                {
                    return false;
                }
                else if (i == x + roundDataPlayer.CountDice[0] - 1)
                {
                    rowUp++;
                    i = x - 1;
                }
            }

            return true;
        }

        public static void GetHeightWidtdArray(string[,] workField, out int height, out int width)
        {
            height = workField.GetUpperBound(0) + 1;
            width = workField.Length / height;
        }

        public static void ChangeColor(ConsoleColor backGroundColor)
        {
            if (backGroundColor == ConsoleColor.Black)
            {
                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            else if (backGroundColor == ConsoleColor.Red)
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.ForegroundColor = ConsoleColor.White;
            }
            else if (backGroundColor == ConsoleColor.Green)
            {
                Console.BackgroundColor = ConsoleColor.Green;
                Console.ForegroundColor = ConsoleColor.Black;
            }
        }

        static void ReplaceRoundIfTrue(FieldSize field, PlayersData playersData, ref RoundData roundDataPlayer)
        {
            if (roundDataPlayer.RoundGame == 1 && playersData.PlayerAreaPoints[0] + playersData.PlayerAreaPoints[1] > field.SquareField / 3)
            {
                roundDataPlayer.RoundGame++;
            }
        }

        static bool CheckPlayerAreaPointsMoreThanSquareFieldDividedTwo(FieldSize field, PlayersData playersData)
        {
            if (playersData.PlayerAreaPoints[0] > field.SquareField / 2 || playersData.PlayerAreaPoints[1] > field.SquareField / 2)
            {
                return true;
            }
            return false;
        }

        static bool CheckField(PlayersData playersData, ref RoundData roundDataPlayer)
        {
            Settings.GetHeightWidtdArray(playersData.PlayersField, out int height, out int width);

            for (int x = 1; x <= width - roundDataPlayer.CountDice[0] + 1; x++)
            {
                for (int y = 1; y <= height - roundDataPlayer.CountDice[1] + 1; y++)
                {
                    if (playersData.PlayersField[height - y, x - 1] == "0" && Settings.CheckArrayForEmptyCell(playersData.PlayersField, roundDataPlayer, x, y))
                    {
                        roundDataPlayer.XTip = x;
                        roundDataPlayer.YTip = y;
                        return true;
                    }
                }
            }

            return false;
        }

        static void RollDice(out int count)
        {
            Random rnd = new Random();
            count = rnd.Next(1, 6);
        }

        static void ShowDice(int count1, int count2, int height)
        {
            int x = 0;
            for (int i = 1; i < 7; i++)
            {

                if (x == 2 && i == count1)
                {
                    Dice(count1, 0, height + 10);
                    Dice(count2, 8, height + 10);
                    Console.WriteLine();
                    break;
                }
                Dice(i, 0, height + 10);
                Dice(i, 8, height + 10);
                Thread.Sleep(80);
                if (i == 6)
                {
                    i = 0;
                    x++;
                }
            }
        }

        static void ReplaceFild(ref PlayersData playersData, RoundData roundDataPlayer, int x, int y)
        {
            int rowUp = 0;
            int height = playersData.PlayersField.GetUpperBound(0) + 1;
            for (int i = x; i < x + roundDataPlayer.CountDice[0] && rowUp < roundDataPlayer.CountDice[1]; i++)
            {
                playersData.PlayersField[height - y - rowUp, i - 1] = $"{roundDataPlayer.Player + 1}";

                if (i == x + roundDataPlayer.CountDice[0] - 1)
                {
                    rowUp++;
                    i = x - 1;
                }
            }
        }

        static void ChangePlayer(ref RoundData roundDataPlayer)
        {
            if (roundDataPlayer.Player == 0)
            {
                roundDataPlayer.Player++;
            }
            else
            {
                roundDataPlayer.Player--;
            }
        }
    }
}
