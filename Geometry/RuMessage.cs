﻿using System;

namespace Geometry
{
    class RuMessage : IMessage
    {
        public void ShowLanguage()
        {
            Console.WriteLine("Вы выбрали русский язык");
        }

        public void RepeatChooseLanguage()
        {
            Console.WriteLine("Выберите правильно язык, клавиша \"1\" или клавишу \"2\"");
        }

        public void SetGabarits()
        {
            Console.WriteLine("Установите размеры игрового поля");
        }

        public void SetWidth()
        {
            Console.WriteLine("Ширина (от 30 до 50): ");
        }

        public void RepeatSetWidth()
        {
            Console.WriteLine("Ширина должна быть от 30 до 50, попробуйте ещё раз: ");
        }

        public void LineStats(int playerAreaPoints, int tip, int chance)
        {
            if (playerAreaPoints < 10)
            {
                Console.Write($" - {playerAreaPoints} очков     ");
                Console.WriteLine($"Подсказок: {tip}    Шансов перекинуть: {chance}");
            }
            else
            {
                Console.Write($" - {playerAreaPoints} очков    ");
                Console.WriteLine($"Подсказок: {tip}    Шансов перекинуть: {chance}");
            }
        }

        public void SetHeight()
        {
            Console.WriteLine("Высота (от 20 до 40): ");
        }

        public void RepeatSetHeight()
        {
            Console.WriteLine("Высота должна быть от 20 до 40, попробуйте ещё раз: ");
        }

        public void PressAnyKey(int player)
        {
            Console.WriteLine($"Бросок {player + 1}-го игрока,  Нажмите на любую клавишу что бы врашать кубики");
        }

        public void NoMovesRollDice()
        {
            Console.WriteLine("Ходов нету, перебрасывайте кубик");
        }

        public void NoMoves()
        {
            Console.WriteLine("Ходов нету...");
        }

        public void SetСoordinates()
        {
            Console.WriteLine("Введите координаты X и Y");
        }

        public void SetСoordinateX()
        {
            Console.Write("X: ");
        }

        public void SetСoordinateY()
        {
            Console.Write("Y: ");
        }

        public void RepeatSetСoordinates(int x, int y)
        {
            Console.WriteLine($"X должно быть меньше {x + 1}, Y должно быть меньше {y + 1}, попробуйте ещё раз:");
        }

        public void RepeatSetСoordinates(int x, int y, ref int attemp)
        {
            if (attemp == 0)
            {
                Console.WriteLine($" X должно быть меньше {x + 1}, Y должно быть меньше {y + 1}, попробуйте ещё раз:");
            }
            else if (attemp == 1)
            {
                Console.WriteLine($"И ещё раз:");
            }
            else if (attemp == 2)
            {
                Console.WriteLine($"Давай, у тебя получится =)");
            }
            else if (attemp == 3)
            {
                Console.WriteLine($"Просто посмотри на поле и введи координаты , куда может поместится твоя фигура");
            }
            else
            {
                Console.WriteLine($"Координаты X и Y - это нижний левый край фигуры");
                attemp = 0;
            }
            attemp++;
        }

        public void CheerUp()
        {
            Random rndCheerUp = new Random();
            int count = rndCheerUp.Next(1, 5);
            string space = "    ";
            if (count == 1)
            {
                Console.WriteLine(space + "Хороший выбор позиции!");
            }
            else if (count == 2)
            {
                Console.WriteLine(space + "Замечательный ход!");
            }
            else if (count == 3)
            {
                Console.WriteLine(space + "Расстановка как у опытного стратега");
            }
            else if (count == 4)
            {
                Console.WriteLine(space + "Я вижу ты не первый день в этой игре, хороший ход!");
            }
            else if (count == 5)
            {
                Console.WriteLine(space + "Хм, почему бы и нет? Посмотрим как на это отреагирует соперник");
            }
            else 
            {
                Console.WriteLine(space + "Даже я бы не додумался до такого хода, Bellissimo!");
            }
        }

        public void UseTip()
        {
            Console.Write("Использовать подсказку?   Клавиша \"1\" - Да  ");
        }

        public void GoodChoice()
        {
            Console.WriteLine("Хороший выбор =)");
        }

        public void Win(int player, int[] square)
        {
            Console.Clear();
            Console.WriteLine();
            Console.WriteLine($"             Победил игрок {player + 1}, со счётом {square[0]} : {square[1]}");
        }
    }
}
