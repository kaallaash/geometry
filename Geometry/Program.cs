﻿using System;

namespace Geometry
{
    class Program
    {   
        static void Main()
        {
            Console.Title = "Geometry";
            Game.StartGame();
            Animation.ShowAK();
            Console.ReadKey();
        }
    }
}