﻿using System;

namespace Geometry
{
    class EngMessage : IMessage
    {
        public void ShowLanguage()
        {
            Console.WriteLine("You have chosen English");
        }

        public void RepeatChooseLanguage()
        {
            Console.WriteLine("Make the right choice, press \"1\" or \"2\"");
        }

        public void SetGabarits()
        {
            Console.WriteLine("Set the size of the playing field");
        }

        public void SetWidth()
        {
            Console.WriteLine("Width (from 30 to 50): ");
        }

        public void RepeatSetWidth()
        {
            Console.WriteLine("The width should be between 30 and 50, try again: ");
        }

        public void SetHeight()
        {
            Console.WriteLine("Height (from 20 to 40): ");
        }

        public void RepeatSetHeight()
        {
            Console.WriteLine("The height should be between 20 and 40, try again: ");
        }

        public void LineStats(int playerAreaPoints, int tip, int chance)
        {
            if (playerAreaPoints < 10)
            {
                Console.Write($" - {playerAreaPoints} points     ");
                Console.WriteLine($"Tip: {tip}    Chance to rotate cubes: {chance}");
            }
            else
            {
                Console.Write($" - {playerAreaPoints} points    ");
                Console.WriteLine($"Tip: {tip}    Chance to rotate cubes: {chance}");
            }
        }

        public void PressAnyKey(int player)
        {
            if (player == 0)
            {
                Console.WriteLine($"{player + 1}st player's throw. Press any key to rotate the cubes");
            }
            else
            {
                Console.WriteLine($"{player + 1}nd player's throw. Press any key to rotate the cubes");
            }
        }

        public void NoMovesRollDice()
        {
            Console.WriteLine("There are no moves throw the cube");
        }

        public void NoMoves()
        {
            Console.WriteLine("There are no moves...");
        }

        public void SetСoordinates()
        {
            Console.WriteLine("Enter the X and Y coordinates");
        }

        public void SetСoordinateX()
        {
            Console.Write("X: ");
        }

        public void SetСoordinateY()
        {   
            Console.Write("Y: ");
        }

        public void RepeatSetСoordinates(int x, int y)
        {
            Console.WriteLine($"X must be less than {x + 1}, Y must be less than {y + 1}, try again:");
        }

        public void RepeatSetСoordinates(int x, int y, ref int attemp)
        {
            if (attemp == 0)
            {
                Console.WriteLine($" X must be less than {x + 1}, Y must be less than {y + 1}, try again:");
            }
            else if (attemp == 1)
            {
                Console.WriteLine($"One more time pls:");
            }
            else if (attemp == 2)
            {
                Console.WriteLine($"Come on, you can do it =)");
            }
            else if (attemp == 3)
            {
                Console.WriteLine($"Just look at the field and enter the coordinates where your figure can fit");
            }
            else
            {
                Console.WriteLine($"The X and Y coordinates are the lower left edge of the shape");
                attemp = 0;
            }
            attemp++;
        }

        public void CheerUp()
        {
            Random rndCheerUp = new Random();
            int count = rndCheerUp.Next(1, 5);
            string space = "    ";
            if (count == 1)
            {
                Console.WriteLine(space + "Good choice of position!");
            }
            else if (count == 2)
            {
                Console.WriteLine(space + "A wonderful move!");
            }
            else if (count == 3)
            {
                Console.WriteLine(space + "The layout is like that of an experienced strategist");
            }
            else if (count == 4)
            {
                Console.WriteLine(space + "I see you are not the first day in this game, good move!");
            }
            else if (count == 5)
            {
                Console.WriteLine(space + "Hmm, why not? Let's see how the opponent reacts to this");
            }
            else
            {
                Console.WriteLine(space + "Even I wouldn't have thought of such a move, Bellissimo!");
            }
        }

        public void UseTip()
        {
            Console.Write("Use a hint? Key \"1\" - Yes ");
        }

        public void GoodChoice()
        {
            Console.WriteLine("Good choice =)");
        }

        public void Win(int player, int[] square)
        {
            Console.Clear();
            Console.WriteLine();
            Console.WriteLine($"             {player + 1} player wins with a score of {square[0]} : {square[1]}");
        }
    }
}
