﻿using System;

namespace Geometry
{
    public class Game
    {
        public static void StartGame()
        {
            IMessage message = Settings.GetMessagesLanguage();
            FieldSize field = Settings.SetGabarits(message);
            PlayersData playersData = Settings.CreatePlayers(field);
            RoundData roundDataPlayer = Settings.CreateRoundData();
            Settings.SetConsoleHeight(field.Height);
            Settings.StartPlay(message, field, roundDataPlayer, playersData);
            Animation.ShowWinAnimation(message, roundDataPlayer, playersData);
            Console.ReadKey();
        }
    }
}
