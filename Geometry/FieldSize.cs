﻿namespace Geometry
{
    public class FieldSize
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public int SquareField { get { return Width * Height; } }
    }
}
